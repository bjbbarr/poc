FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/poc-0.0.1-SNAPSHOT.jar target/poc-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","target/poc-0.0.1-SNAPSHOT.jar"]