package com.poc.poc;

import org.joda.time.Instant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.Random;

@SpringBootApplication
@RestController
public class PocApplication {

	private static final Integer NUM_MESSAGES = 3;

	@RequestMapping("/insert")
	public String insert() {


		// Connect to vtgate.
		String dbURL = "jdbc:vitess://" + "vtgate-test:15991";
		String userName = "mysql_user";
		String password = "mysql_password";
		try (Connection conn = DriverManager.getConnection(dbURL, userName, password)) {


			conn.setAutoCommit(false);

			// Insert some messages on random pages.
			System.out.println("Inserting into master...");
			try (PreparedStatement stmt = conn.prepareStatement(
					"INSERT INTO messages (page,time_created_ns,message) VALUES (?,?,?)")) {
				for (int i = 0; i < NUM_MESSAGES; i++) {

					Random rand = new Random();
					Instant timeCreated = Instant.now();
					int page = rand.nextInt(100) + 1;
					stmt.setInt(1, page);
					stmt.setLong(2, timeCreated.getMillis() * 1000000);
					stmt.setString(3, "Message Number " + i);
					stmt.execute();
				}
			}

			// To Commit Open Transaction
			conn.commit();

		} catch (Exception e) {
			System.out.println("Vitess JDBC example failed.");
			System.out.println("Error Details:");
			e.printStackTrace();
			System.exit(2);
		}

		return "inserted " + NUM_MESSAGES + " messages";
	}

	@RequestMapping("/read")
	public String read() {

		StringBuilder outputString = new StringBuilder();
		outputString.append("Messages: ");

		String sql = "SELECT page, time_created_ns, message FROM messages";

		String dbURL = "jdbc:vitess://" + "vtgate-test:15991";
		String userName = "mysql_user";
		String password = "mysql_password";

		try (Connection conn = DriverManager.getConnection(dbURL, userName, password)) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				long page = rs.getLong("page");
				long timeCreated = rs.getLong("time_created_ns");
				String message = rs.getString("message");
				System.out.format("(%s, %s, %s)\n", page, timeCreated, message);
				outputString.append(message + " ");
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return outputString.toString();
	}

	public static void main(String[] args) {
		SpringApplication.run(PocApplication.class, args);
	}

}